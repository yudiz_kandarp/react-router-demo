import React from 'react'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { doLogout, isLoggedIn } from '../../Auth/Auth'

function Header() {
	const navigate = useNavigate()
	const path = useLocation().pathname
	function activePath(cPath) {
		return path == cPath
	}
	return (
		<>
			<header>
				{isLoggedIn() ? (
					<>
						<Link
							to='/'
							style={{ color: `${activePath('/') ? 'yellow' : '#fff'}` }}
						>
							Home
						</Link>
						<div
							onClick={() => {
								navigate('/login')
								doLogout()
							}}
						>
							Logout
						</div>
					</>
				) : (
					<div style={{ display: 'flex' }}>
						<div
							onClick={() => {
								navigate('/signup')
								doLogout()
							}}
							style={{ color: `${activePath('/signup') ? 'yellow' : '#fff'}` }}
						>
							signup
						</div>
						<div
							onClick={() => {
								navigate('/login')
								doLogout()
							}}
							style={{ color: `${activePath('/login') ? 'yellow' : '#fff'}` }}
						>
							login
						</div>
					</div>
				)}
			</header>
		</>
	)
}

export default Header
