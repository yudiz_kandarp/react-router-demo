/* Components */
import Card from './Card/Card'
import CardContainer from './CardContainer/CardContainer'
import Header from './Header/Header'
import SinglePost from './SinglePost/SinglePost'

export { Card, Header, SinglePost, CardContainer }

/* card style */
import '../Styles/card.scss'
/* Header style */
import '../Styles/header.scss'
/* SinglePost style */
import '../Styles/singlepost.scss'
/* Login page style */
import '../Styles/login.scss'
