import React from 'react'
import PropTypes from 'prop-types'
import { Card } from '../index'

function CardContainer({ item: data }) {
	return (
		<div className='card_container'>
			{data?.map((item) => (
				<Card key={item.id} item={item} />
			))}
		</div>
	)
}
CardContainer.propTypes = {
	item: PropTypes.array,
}
export default CardContainer
