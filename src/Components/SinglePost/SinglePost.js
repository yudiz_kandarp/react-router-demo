import React from 'react'
import PropTypes from 'prop-types'
import { useNavigate } from 'react-router-dom'
function SinglePost({ item }) {
	const navigate = useNavigate()
	return (
		<>
			<div
				className='singlePost'
				style={{
					backgroundImage: `url("https://image.tmdb.org/t/p/w400${item.backdrop_path}")`,
				}}
			></div>
			<div className='singlepost_detail_container'>
				<img
					className='singlePost_img'
					src={`https://image.tmdb.org/t/p/w200${item.poster_path}`}
					alt={item.title}
				/>
				<div className='singlePost_description'>
					<h1 className='description'>{item.title}</h1>
					<p className='description'>{item.overview}</p>
				</div>
			</div>
			<div className='back-btn' onClick={() => navigate(-1)}></div>
		</>
	)
}
SinglePost.propTypes = {
	item: PropTypes.object,
}
export default SinglePost
