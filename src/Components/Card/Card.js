import React from 'react'
import PropTypes from 'prop-types'
import { Link, useNavigate } from 'react-router-dom'
function Card({ item }) {
	const navigate = useNavigate()

	return (
		<div>
			<div className='card'>
				<div className='card_img_container'>
					<Link to={`/posts/${item.id}`}>
						<img
							src={`https://image.tmdb.org/t/p/w200/${item.poster_path}`}
							alt={item.title}
							className='card_img'
						/>
					</Link>
				</div>
				<div className='card_details'>
					<Link to={`/posts/${item.id}`} className='title'>
						{item.title}
					</Link>
					<div
						className='description'
						onClick={() => {
							navigate(`/posts/${item.id}`)
						}}
					>
						{item.overview.slice(0, 170) + '...'}
					</div>
				</div>
			</div>
		</div>
	)
}
Card.propTypes = {
	item: PropTypes.object,
}
export default Card
