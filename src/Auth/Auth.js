export function doLogin(data) {
	const allData = JSON.parse(localStorage.getItem('db')) || []
	const user = allData.find(
		(item) => item.email == data.email && item.password == data.password
	)
	if (!user) return false
	localStorage.setItem('auth', JSON.stringify(user))
	return true
}

export function doLogout() {
	localStorage.removeItem('auth')
}
export function doSignup(data) {
	const allData = JSON.parse(localStorage.getItem('db')) || []
	const user = allData.find((item) => item.email == data.email)
	if (user) return false
	localStorage.setItem('auth', JSON.stringify(data))
	localStorage.setItem('db', JSON.stringify([...allData, data]))
	return true
}

export function isLoggedIn() {
	if (!localStorage.getItem('auth')) return false
	return true
}
