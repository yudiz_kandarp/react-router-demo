import React from 'react'
import { useParams } from 'react-router-dom'
import PropTypes from 'prop-types'
import { SinglePost } from '../Components'
function SinglePostPage({ item }) {
	const { postId } = useParams()
	const singlePost = item?.find((post) => post.id == postId)
	return (
		<div>{singlePost ? <SinglePost item={singlePost} /> : 'Loading...'}</div>
	)
}
SinglePostPage.propTypes = {
	item: PropTypes.array,
}

export default SinglePostPage
