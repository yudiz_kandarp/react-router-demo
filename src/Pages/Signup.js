import React, { useRef } from 'react'
import { useNavigate } from 'react-router-dom'
import { doSignup } from '../Auth/Auth'

function Signup() {
	const email = useRef(null)
	const name = useRef(null)
	const password = useRef(null)
	const navigate = useNavigate()

	function handleSubmit(e) {
		e.preventDefault()
		if (
			doSignup({
				email: email.current.value,
				name: name.current.value,
				password: password.current.value,
			})
		) {
			navigate('/')
		} else {
			email.current.value = ''
			name.current.value = ''
			password.current.value = ''
			alert('user already exist with this email')
		}
	}

	return (
		<div className='form_container'>
			<h1>signup</h1>
			<form onSubmit={handleSubmit}>
				<input
					ref={email}
					type='text'
					className='input'
					placeholder='email'
					defaultValue={email.current?.value || ''}
					required
				/>
				<input
					ref={name}
					type='text'
					className='input'
					placeholder='name'
					defaultValue={name.current?.value || ''}
					required
				/>
				<input
					ref={password}
					type='password'
					className='input'
					placeholder='password'
					defaultValue={password.current?.value || ''}
					required
				/>
				<button> Signup</button>
			</form>
		</div>
	)
}

export default Signup
