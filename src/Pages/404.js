import React from 'react'

function PageNotFound() {
	return (
		<div
			style={{
				display: 'flex',
				alignItems: 'center',
				justifyContent: 'center',
				height: '80vh',
			}}
		>
			<h1>Page Not Found</h1>
		</div>
	)
}

export default PageNotFound
