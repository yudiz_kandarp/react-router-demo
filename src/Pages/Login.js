import React, { useRef } from 'react'
import { useNavigate } from 'react-router-dom'
import { doLogin } from '../Auth/Auth'

function Login() {
	const email = useRef(null)
	const password = useRef(null)
	const navigate = useNavigate()

	function handleSubmit(e) {
		e.preventDefault()
		if (
			!doLogin({
				email: email.current.value,
				password: password.current.value,
			})
		) {
			email.current.value = ''
			password.current.value = ''
			email.current.focus()
			return alert('invalid credentials')
		}
		navigate('/')
	}

	return (
		<div className='form_container'>
			<h1>Login</h1>
			<form onSubmit={handleSubmit}>
				<input
					ref={email}
					type='text'
					className='input'
					placeholder='email'
					defaultValue={email.current?.value || ''}
					required
				/>
				<input
					ref={password}
					type='password'
					className='input'
					placeholder='password'
					defaultValue={password.current?.value || ''}
					required
				/>
				<button> Login</button>
			</form>
		</div>
	)
}

export default Login
