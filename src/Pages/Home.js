import React from 'react'
import { CardContainer } from '../Components'

function Home(props) {
	return (
		<div>
			<CardContainer {...props} />
		</div>
	)
}

export default Home
