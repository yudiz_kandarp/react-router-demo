import React, { useEffect, useState } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Header } from './Components'
import PageNotFound from './Pages/404'
import Home from './Pages/Home'
import SinglePostPage from './Pages/SinglePostPage'
import data from './Data/db.json'
import Login from './Pages/Login'
import Private from './Routes/Private'
import Public from './Routes/Public'
import Signup from './Pages/Signup'

function App() {
	const [movies, setMovies] = useState()
	useEffect(() => {
		setMovies(data)
	}, [data])
	return (
		<BrowserRouter>
			<Header />
			<Routes>
				<Route
					exact
					path='/'
					element={
						<Private>
							<Home item={movies} />
						</Private>
					}
				/>

				<Route
					path='/login'
					element={
						<Public>
							<Login />
						</Public>
					}
				/>
				<Route
					path='/signup'
					element={
						<Public>
							<Signup />
						</Public>
					}
				/>
				<Route
					path='/posts/:postId'
					element={
						<Private>
							<SinglePostPage item={movies} />
						</Private>
					}
				/>
				<Route path='*' element={<PageNotFound />} />
			</Routes>
		</BrowserRouter>
	)
}

export default App
