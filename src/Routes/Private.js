import React from 'react'
import PropTypes from 'prop-types'
import { isLoggedIn } from '../Auth/Auth'
import { Navigate } from 'react-router-dom'

function Private({ children }) {
	if (!isLoggedIn()) return <Navigate to={'/login'} replace />
	return <div>{children}</div>
}
Private.propTypes = {
	children: PropTypes.node,
}
export default Private
