import React from 'react'
import PropTypes from 'prop-types'
import { isLoggedIn } from '../Auth/Auth'
import { Navigate } from 'react-router-dom'
function Public({ children }) {
	if (!isLoggedIn()) return <div>{children}</div>
	return <Navigate to='/' replace />
}
Public.propTypes = {
	children: PropTypes.node,
}
export default Public
